import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.G_SiteURL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Page_Upload/a_File Upload'))

userDir = System.getProperty('user.dir')

filePath = (userDir + '\\Data Files\\File Upload\\')

String concatenate = ''

for (int i = 1; i <= findTestData(GlobalVariable.dataFile).getRowNumbers(); i++) {
    if (i == findTestData(GlobalVariable.dataFile).getRowNumbers()) {
        concatenate += (filePath + findTestData(GlobalVariable.dataFile).getValue(1, i))
    } else {
        concatenate += ((filePath + findTestData(GlobalVariable.dataFile).getValue(1, i)) + '\n')
    }
}

WebUI.uploadFileWithDragAndDrop(findTestObject('Object Repository/Page_Upload/area_DragAndDrop'), concatenate)

WebUI.waitForElementPresent(findTestObject('Object Repository/Page_Upload/span_picture1.png'), 5)

WebUI.waitForElementPresent(findTestObject('Object Repository/Page_Upload/span_picture.png'), 5)

WebUI.waitForElementPresent(findTestObject('Object Repository/Page_Upload/span_picture2.png'), 5)

WebUI.closeBrowser()

