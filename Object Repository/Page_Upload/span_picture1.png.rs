<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_picture1.png</name>
   <tag></tag>
   <elementGuidId>cacaa072-ef39-4309-a434-25117cc66d14</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='drag-drop-upload']/div/div/div/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'picture1.png' or . = 'picture1.png')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>24a53e1a-d50c-4e87-83cf-0ad311cc5729</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>picture1.png</value>
      <webElementGuid>4f22e9be-3a5f-4f1a-9f46-e98975a1387f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;drag-drop-upload&quot;)/div[@class=&quot;dz-preview dz-processing dz-image-preview dz-success dz-complete&quot;]/div[@class=&quot;dz-details&quot;]/div[@class=&quot;dz-filename&quot;]/span[1]</value>
      <webElementGuid>85351f71-11d4-4620-ae48-0def691fce44</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='drag-drop-upload']/div/div/div/span</value>
      <webElementGuid>c543c218-8999-4177-82e3-bb7e8742e92f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='File Uploader'])[1]/following::span[1]</value>
      <webElementGuid>2cf7f4c8-9a0c-42e7-b422-e721a3b1eec9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='✔'])[1]/preceding::span[1]</value>
      <webElementGuid>eff5d10d-d2b7-42a1-9db6-46413890aec2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='picture2.png'])[1]/preceding::span[2]</value>
      <webElementGuid>7f5682e1-5b89-4855-aec8-0b87ffe7c26f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='picture1.png']/parent::*</value>
      <webElementGuid>da18ba35-37f4-43e8-be7a-d71aef37b560</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span</value>
      <webElementGuid>42763fca-4031-43a1-b5ce-f097d8962f8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'picture1.png' or . = 'picture1.png')]</value>
      <webElementGuid>a653b839-5f1f-4640-9162-08c8c9dfab8d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
